const express = require('express');
/* eslint new-cap: ["error", { "capIsNewExceptions": ["Router"] }] */
const router = express.Router();

const {
  getTrucks,
  createTruck,
  get1Truck,
  updateTruck,
  deleteTruck,
  assignTruck,
} = require('../controllers/trucksController');

const {protect} = require('../middleware/authMiddleware');

router.route('/').get(protect, getTrucks).post(protect, createTruck);
router.route('/:id')
    .get(protect, get1Truck)
    .put(protect, updateTruck)
    .delete(protect, deleteTruck);
router.route('/:id/assign').post(protect, assignTruck);

module.exports = router;
