const express = require('express');
/* eslint new-cap: ["error", { "capIsNewExceptions": ["Router"] }] */
const router = express.Router();

const {
  getLoads,
  createLoad,
  getActiveLoad,
  activeNextState,
  get1Load,
  updateLoad,
  deleteLoad,
  postLoad,
  getInfo,
} = require('../controllers/loadsController');

const {protect} = require('../middleware/authMiddleware');

router.route('/').get(protect, getLoads).post(protect, createLoad);
router.route('/active').get(protect, getActiveLoad);
router.route('/active/state').patch(protect, activeNextState);
router.route('/:id')
    .get(protect, get1Load)
    .put(protect, updateLoad)
    .delete(protect, deleteLoad);
router.route('/:id/post').post(protect, postLoad);
router.route('/:id/shipping_info').get(protect, getInfo);


module.exports = router;

