const express = require('express');
/* eslint new-cap: ["error", { "capIsNewExceptions": ["Router"] }] */
const router = express.Router();

const {
  registerUser,
  loginUser,
  forgotPassword,
  getUser,
  deleteUser,
  changePassword,
} = require('../controllers/userController');

const {protect} = require('../middleware/authMiddleware');

router.post('/auth/register', registerUser);
router.post('/auth/login', loginUser);
router.post('/auth/forgot_password', forgotPassword);

router.get('/users/me', protect, getUser);
router.delete('/users/me', protect, deleteUser);
router.patch('/users/me/password', protect, changePassword);

module.exports = router;
