const express = require('express');
require('dotenv').config();
const {errorHandler} = require('./middleware/errorMiddleware');
const connectDB = require('./config/db');
const port = process.env.PORT || 8080;

connectDB();

const app = express();

app.use(express.json());
app.use(express.urlencoded({extended: false}));

app.use('/api/trucks', require('./routes/trucksRoutes'));
app.use('/api', require('./routes/userRoutes'));
app.use('/api/loads', require('./routes/loadsRoutes'));

app.use(errorHandler);

app.listen(port, () => console.log(`Server started on port ${port}`));
