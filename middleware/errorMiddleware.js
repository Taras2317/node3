const errorHandler = (err, req, res, next) => {
  const statusCode = res.statusCode ? res.statusCode : 500;

  res.status(statusCode);

  const fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;

  console.log(statusCode, ` ${req.method}  ${fullUrl}
Error: ${err.message}`);

  res.json({
    message: err.message,
  });
};

module.exports = {
  errorHandler,
};
