const asyncHandler = require('express-async-handler');
const Truck = require('../models/truckModel');
const User = require('../models/userModel');

const truckTypes = ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'];

const fullUrl = (req) => {
  return req.protocol + '://' + req.get('host') + req.originalUrl;
};

const checkRole = (role, res) => {
  if (role !== 'DRIVER') {
    res.status(400);
    throw new Error(`Access denied. Only user with role 'DRIVER' can access`);
  }
};

const ifTruckNotExistErr = (truck, res) => {
  if (!truck) {
    res.status(400);
    throw new Error('Truck not found');
  }
};


// @desc    Get trucks
// @route   GET /api/trucks
// @access  Private
const getTrucks = asyncHandler(async (req, res) => {
  checkRole(req.user.role, res);

  const trucks = await Truck.find({created_by: req.user.id});

  res.status(200).json({trucks});
  console.log(200, ` ${req.method}:  ${fullUrl(req)}
Success get trucks`);
});


// @desc    Add truck
// @route   POST /api/trucks
// @access  Private
const createTruck = asyncHandler(async (req, res) => {
  checkRole(req.user.role, res);

  if (!req.body.type) {
    res.status(400);
    throw new Error(`Field 'type' is required`);
  }

  if (!truckTypes.includes(req.body.type)) {
    res.status(400);
    throw new Error(`Invalid '${req.body.type}' truck type`);
  }

  await Truck.create({
    created_by: req.user.id,
    assigned_to: '',
    type: req.body.type,
    status: 'IS',
    created_date: Date.now(),
  });

  res.status(200).json({message: 'Truck created successfully'});
  console.log(200, `${req.method}: ${fullUrl(req)}
Truck created successfully`);
});


// @desc    Get truck by id
// @route   GET /api/trucks/:id
// @access  Private
const get1Truck = asyncHandler(async (req, res) => {
  checkRole(req.user.role, res);

  const truck = await Truck.findById(req.params.id);
  ifTruckNotExistErr(truck, res);

  res.status(200).json({truck});
  console.log(200, `${req.method}: ${fullUrl(req)}
Success get truck
Truck: ${truck}`);
});


// @desc    Update truck by id
// @route   PUT /api/trucks/:id
// @access  Private
const updateTruck = asyncHandler(async (req, res) => {
  checkRole(req.user.role, res);

  const truck = await Truck.findById(req.params.id);
  ifTruckNotExistErr(truck, res);

  if (truck.assigned_to !== '') {
    res.status(400);
    throw new Error(`You can't update assigned truck`);
  }

  if (!req.body.type) {
    res.status(400);
    throw new Error(`Field 'type' is required`);
  }

  if (!truckTypes.includes(req.body.type)) {
    res.status(400);
    throw new Error(`Invalid '${req.body.type}' truck type`);
  }

  const user = await User.findById(req.user.id);
  if (!user) {
    res.status(400);
    throw new Error('User not found');
  }
  // Make sure logged in user matches truck user
  if (truck.created_by.toString() !== user.id) {
    res.status(400);
    throw new Error('User not authorized');
  }

  await Truck.findByIdAndUpdate(req.params.id, req.body, {new: true});

  res.status(200).json({message: 'Truck details changed successfully'});
  console.log(200, `${req.method}: ${fullUrl(req)}
Truck details changed successfully
Truck id: ${req.params.id}
New type: ${req.body.type}`);
});


// @desc    Delete truck by id
// @route   DELETE /api/trucks/:id
// @access  Private
const deleteTruck = asyncHandler(async (req, res) => {
  checkRole(req.user.role, res);

  const truck = await Truck.findById(req.params.id);
  ifTruckNotExistErr(truck, res);

  if (truck.assigned_to !== '') {
    res.status(400);
    throw new Error(`You can't delete assigned truck`);
  }

  const user = await User.findById(req.user.id);
  if (!user) {
    res.status(400);
    throw new Error('User not found');
  }
  // Make sure logged in user matches truck user
  if (truck.created_by.toString() !== user.id) {
    res.status(400);
    throw new Error('User not authorized');
  }

  await truck.remove();
  res.status(200).json({message: 'Truck deleted successfully'});
  console.log(200, `${req.method}: ${fullUrl(req)}
Truck deleted successfully
Truck id: ${req.params.id}`);
});


// @desc    Assign truck by id
// @route   POST /api/trucks/:id/assign
// @access  Private
const assignTruck = asyncHandler(async (req, res) => {
  checkRole(req.user.role, res);

  const assignedTruck = await Truck.find({assigned_to: req.user.id});
  if (assignedTruck.length > 0) {
    res.status(400);
    throw new Error('User already has assigned truck');
  };

  const truck = await Truck.findById(req.params.id);
  ifTruckNotExistErr(truck, res);

  await Truck.findByIdAndUpdate(req.params.id, {assigned_to: req.user.id});

  res.status(200).json({message: 'Truck assigned successfully'});
  console.log(200, `${req.method}: ${fullUrl(req)}
Truck assigned successfully
Truck id: ${req.params.id}`);
});

module.exports = {
  getTrucks,
  createTruck,
  get1Truck,
  updateTruck,
  deleteTruck,
  assignTruck,
};

