const asyncHandler = require('express-async-handler');
const Truck = require('../models/truckModel');
const User = require('../models/userModel');
const Load = require('../models/loadModel');

const fullUrl = (req) => {
  return req.protocol + '://' + req.get('host') + req.originalUrl;
};
// Check if user role is SHIPPER
const checkRoleShipper = (role, res) => {
  if (role !== 'SHIPPER') {
    res.status(400);
    throw new Error(`Access denied. Only user with role 'SHIPPER' can access`);
  }
};
// Check if user role is DRIVER
const checkRoleDriver = (role, res) => {
  if (role !== 'DRIVER') {
    res.status(400);
    throw new Error(`Access denied. Only user with role 'DRIVER' can access`);
  }
};

// @desc    Get user's loads
// @route   GET /api/loads
// @access  Private
const getLoads = asyncHandler(async (req, res) => {
  const status = req.query.status;
  let limit = parseInt(req.query.limit) || 10;
  if (limit > 50) {
    limit = 50;
  }
  const offset = parseInt(req.query.offset) || 0;

  let loads = await Load.find({created_by: req.user.id})
      .limit(limit).skip(offset);

  if (status) {
    loads = loads.filter((load) => load.status === status);
  }

  res.status(200).json({loads});
  console.log(200, ` ${req.method}:  ${fullUrl(req)}
Successfully got loads`);
});


// @desc    Add load for user
// @route   POST /api/loads
// @access  Private
const createLoad = asyncHandler(async (req, res) => {
  checkRoleShipper(req.user.role, res);

  const {name, payload, dimensions} = req.body;
  const pickupAddress = req.body.pickup_address;
  const deliveryAddress = req.body.delivery_address;

  if (!name || !payload || !pickupAddress || !deliveryAddress || !dimensions) {
    res.status(400);
    throw new Error('Please fill all the fields');
  }

  await Load.create({
    created_by: req.user.id,
    assigned_to: '',
    status: 'NEW',
    state: '',
    name: name,
    payload: payload,
    pickup_address: pickupAddress,
    delivery_address: deliveryAddress,
    dimensions: dimensions,
    logs: [{
      message: 'Load created',
      time: Date.now(),
    }],
    created_date: Date.now(),
  });

  res.status(200).json({message: 'Load created successfully'});
  console.log(200, ` ${req.method}:  ${fullUrl(req)}
Load created successfully`);
});


// @desc    Get user's active load(if exist)
// @route   GET /api/loads/active
// @access  Private
const getActiveLoad = asyncHandler(async (req, res) => {
  checkRoleDriver(req.user.role, res);

  const user = await User.findById(req.user.id);
  if (!user) {
    res.status(400);
    throw new Error('User not found');
  };

  const load = await Load.findOne({assigned_to: user.id});
  if (!load) {
    res.status(400);
    throw new Error('Load not found');
  };

  res.status(200).json({load});
  console.log(200, ` ${req.method}:  ${fullUrl(req)}
Success get active load`);
});


// @desc    Iterate to next load state
// @route   PATCH /api/loads/active/state
// @access  Private
const activeNextState = asyncHandler(async (req, res) => {
  checkRoleDriver(req.user.role, res);

  const user = await User.findById(req.user.id);
  if (!user) {
    res.status(400);
    throw new Error('User not found');
  };

  const load = await Load.findOne({assigned_to: user.id});
  if (!load) {
    res.status(400);
    throw new Error('Load not found');
  };

  const loadStates = [
    'En route to Pick Up',
    'Arrived to Pick Up',
    'En route to delivery',
    'Arrived to delivery',
  ];

  const currentState = load.state;
  const index = loadStates.indexOf(currentState);
  const newState = loadStates[index + 1];

  await Load.findOneAndUpdate({assigned_to: user.id}, {state: newState});

  res.status(200).json({
    message: `Load state changed to '${newState}'`,
  });
  console.log(200, ` ${req.method}:  ${fullUrl(req)}
Load state changed to '${newState}'`);
});


// @desc    Get user's load by id
// @route   GET /api/loads/:id
// @access  Private
const get1Load = asyncHandler(async (req, res) => {
  const load = await Load.findById(req.params.id);

  if (!load) {
    res.status(400);
    throw new Error('Load not found');
  }

  res.status(200).json({load});
  console.log(200, ` ${req.method}:  ${fullUrl(req)}
Success get load`);
});


// @desc    Update user's load by id
// @route   PUT /api/loads/:id
// @access  Private
const updateLoad = asyncHandler(async (req, res) => {
  checkRoleShipper(req.user.role, res);

  const load = await Load.findById(req.params.id);
  if (!load) {
    res.status(400);
    throw new Error('Load not found');
  }
  console.log(load.status);
  if (load.status !== 'NEW') {
    res.status(400);
    throw new Error(`You can't update load with status other than 'NEW'`);
  }

  const {name, payload, dimensions} = req.body;
  const pickupAddress = req.body.pickup_address;
  const deliveryAddress = req.body.delivery_address;
  if (!name || !payload || !pickupAddress || !deliveryAddress || !dimensions) {
    res.status(400);
    throw new Error('Please fill all the fields');
  }

  const user = await User.findById(req.user.id);
  if (!user) {
    res.status(400);
    throw new Error('User not found');
  }

  // Make sure logged in user matches load user
  if (load.created_by.toString() !== user.id) {
    res.status(400);
    throw new Error('User not authorized');
  }

  await Load.findByIdAndUpdate(req.params.id, req.body, {new: true});

  res.status(200).json({message: 'Load details changed successfully'});
  console.log(200, ` ${req.method}:  ${fullUrl(req)}
Load details changed successfully
id: ${req.params.id}`);
});


// @desc    Delete user's load by id
// @route   DELETE /api/loads/:id
// @access  Private
const deleteLoad = asyncHandler(async (req, res) => {
  checkRoleShipper(req.user.role, res);

  const load = await Load.findById(req.params.id);
  if (!load) {
    res.status(400);
    throw new Error('Load not found');
  }

  if (load.status !== 'NEW') {
    res.status(400);
    throw new Error(`You can't delete load with status other than 'NEW'`);
  }

  const user = await User.findById(req.user.id);
  if (!user) {
    res.status(400);
    throw new Error('User not found');
  }

  // Make sure logged in user matches load user
  if (load.created_by.toString() !== user.id) {
    res.status(400);
    throw new Error('User not authorized');
  }

  await load.remove();

  res.status(200).json({message: 'Load deleted successfully'});
  console.log(200, ` ${req.method}:  ${fullUrl(req)}
Load deleted successfully`);
});


// @desc    Post user's load by id
// @route   POST /api/loads/:id/post
// @access  Private
const postLoad = asyncHandler(async (req, res) => {
  checkRoleShipper(req.user.role, res);

  const load = await Load.findById(req.params.id);
  if (!load) {
    res.status(400);
    throw new Error('Load not found');
  }

  const user = await User.findById(req.user.id);
  if (!user) {
    res.status(400);
    throw new Error('User not found');
  }

  // Make sure logged in user matches load user
  if (load.created_by.toString() !== user.id) {
    res.status(400);
    throw new Error('User not authorized');
  }

  let car;
  if (load.dimensions.length <= 700 &&
    load.dimensions.height <= 350 &&
    load.dimensions.width <= 200 &&
    load.payload < 4000) {
    car = 'LARGE STRAIGHT';
  }
  if (load.dimensions.length <= 500 &&
    load.dimensions.height <= 250 &&
    load.dimensions.width <= 170 &&
    load.payload < 2500) {
    car = 'SMALL STRAIGHT';
  }
  if (load.dimensions.length <= 300 &&
    load.dimensions.height <= 250 &&
    load.dimensions.width <= 170 &&
    load.payload < 1700) {
    car = 'SPRINTER';
  }
  await Load.findByIdAndUpdate(req.params.id, {status: 'POSTED'});

  const trucks = await Truck.find({status: 'IS', type: car});

  if (trucks.length === 0) {
    await Load.findByIdAndUpdate(req.params.id, {status: 'NEW'});
    await Load.updateOne(
        {_id: req.params.id},
        {$push: {logs: {message: 'Driver not found', time: Date.now()}}},
    );
    res.status(400);
    throw new Error('Driver and truck needed not found');
  }

  const isDriverFound = trucks.some((truck) => {
    return truck.assigned_to !== '';
  });

  if (isDriverFound) {
    const findedTruck = trucks.filter((truck) => truck.assigned_to !== '');

    await Truck.findByIdAndUpdate(findedTruck[0].id, {status: 'OL'});
    await Load.findByIdAndUpdate(req.params.id, {
      assigned_to: findedTruck[0].assigned_to});
    await Load.findByIdAndUpdate(req.params.id, {status: 'ASSIGNED'});
    await Load.findByIdAndUpdate(req.params.id, {state: 'En route to Pick Up'});
    await Load.updateOne(
        {_id: req.params.id},
        {$push: {logs: {
          message: 'Truck found and assigned', time: Date.now(),
        }}},
    );

    res.status(200).json({
      message: 'Load posted successfully',
      driver_found: isDriverFound,
    });
    console.log(200, ` ${req.method}:  ${fullUrl(req)}
Load posted successfully`);
  } else {
    await Load.findByIdAndUpdate(req.params.id, {status: 'NEW'});
    await Load.updateOne(
        {_id: req.params.id},
        {$push: {logs: {message: 'Driver not found', time: Date.now()}}},
    );
    res.status(400);
    throw new Error('Driver and truck needed not found');
  }
});


// @desc    Get user's load shipping info by id
// @route   GET /api/loads/:id/shipping_info
// @access  Private
const getInfo = asyncHandler(async (req, res) => {
  checkRoleShipper(req.user.role, res);

  const load = await Load.findById(req.params.id);
  if (!load) {
    res.status(400);
    throw new Error('Load not found');
  }

  const truck = await Truck.find({assigned_to: load.assigned_to});

  res.status(200).json({load, truck});
  console.log(200, ` ${req.method}:  ${fullUrl(req)}
Success get shipping info`);
});


module.exports = {
  getLoads,
  createLoad,
  getActiveLoad,
  activeNextState,
  get1Load,
  updateLoad,
  deleteLoad,
  postLoad,
  getInfo,
};
