const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const asyncHandler = require('express-async-handler');
const User = require('../models/userModel');

const roles = ['SHIPPER', 'DRIVER'];

const fullUrl = (req) => {
  return req.protocol + '://' + req.get('host') + req.originalUrl;
};


// @desc    Register new user
// @route   POST /auth/register
// @access  Public
const registerUser = asyncHandler(async (req, res) => {
  const {email, password, role} = req.body;

  if (!email || !password || !role) {
    res.status(400);
    throw new Error('Please fill up all the fields');
  }

  if (!roles.includes(req.body.role)) {
    res.status(400);
    throw new Error(`Wrong role, please select between 'SHIPPER' or 'DRIVER'`);
  }
  // Check if user exists
  const userExists = await User.findOne({email});
  if (userExists) {
    res.status(400);
    throw new Error('User already exists');
  }
  // Hash password
  const salt = await bcrypt.genSalt(10);
  const hashedPassword = await bcrypt.hash(password, salt);

  // Create user
  const user = await User.create({
    role,
    email,
    password: hashedPassword,
    created_date: Date.now(),
  });

  if (user) {
    res.status(200).json({message: 'Profile created successfully'});
    console.log(200, `${req.method} ${fullUrl(req)}
Profile created successfully`);
  } else {
    res.status(400);
    throw new Error('Invalid user data');
  }
});


// @desc    Login user
// @route   POST /auth/login
// @access  Public
const loginUser = asyncHandler(async (req, res) => {
  const {email, password} = req.body;

  if (!email || !password) {
    res.status(400);
    throw new Error('Please fill all the fields');
  }

  // Check if user with this email exists
  const user = await User.findOne({email});

  if (user && (await bcrypt.compare(password, user.password))) {
    res.status(200).json({
      jwt_token: generateToken(user._id, user.role),
    });

    console.log(200, `${req.method} ${fullUrl(req)}
Successful login`);
  } else {
    res.status(400);
    throw new Error('Invalid credentials');
  }
});


// @desc    Forgot password
// @route   POST /auth/forgot_password
// @access  Public
const forgotPassword = asyncHandler(async (req, res) => {
  const email = req.body.email;
  if (!email) {
    res.status(400);
    throw new Error(`Please fill the 'email' field`);
  }

  const user = await User.findOne({email});
  if (user) {
    res.status(200).json({message: 'New password sent to your email address'});

    console.log(200, `${req.method} ${fullUrl(req)}
Successfully reset password`);
  } else {
    res.status(400);
    throw new Error('Invalid credentials');
  }
});


// @desc    Get user
// @route   GET /users/me
// @access  Private
const getUser = asyncHandler(async (req, res) => {
  const user = await User.findById(req.user?.id);
  if (!user) {
    res.status(400);
    throw new Error('User not found');
  }

  const {_id, role, email} = await User.findById(req.user.id);

  res.status(200).json({
    user: {
      _id,
      role,
      email,
      created_date: req.user.created_date,
    },
  });
  console.log(200, `${req.method} ${fullUrl(req)}
Success get user`);
});


// @desc    Delete user
// @route   DELETE /users/me
// @access  Private
const deleteUser = asyncHandler(async (req, res) => {
  const user = await User.findById(req.user?.id);

  if (!user) {
    res.status(400);
    throw new Error('User dont exists');
  }

  await user.remove();

  res.status(200).json({message: 'Profile deleted successfully'});
  console.log(200, `${req.method} ${fullUrl(req)}
Profile deleted successfully`);
});


// @desc    Change password
// @route   PATCH /users/me/password
// @access  Private
const changePassword = asyncHandler(async (req, res) => {
  const {oldPassword, newPassword} = req.body;
  // Find user with password field in db
  const user = await User.findById(req.user.id);

  if (!oldPassword || !newPassword) {
    res.status(400);
    throw new Error('Please fill all the fields');
  }

  const salt = await bcrypt.genSalt(10);
  const newHashedPassword = await bcrypt.hash(newPassword, salt);

  if (await bcrypt.compare(oldPassword, user.password)) {
    await User.findByIdAndUpdate(user.id, {password: newHashedPassword});

    res.status(200);
    res.json({message: 'Password changed successfully'});

    console.log(200, `${req.method} ${fullUrl(req)}
Password changed successfully`);
  } else {
    res.status(400);
    throw new Error('Wrong old password');
  }
});

// Generate JWT
const generateToken = (id, role) => {
  return jwt.sign({id, role}, process.env.JWT_SECRET, {
    expiresIn: '30d',
  });
};

module.exports = {
  registerUser,
  loginUser,
  forgotPassword,
  getUser,
  deleteUser,
  changePassword,
};
