const mongoose = require('mongoose');

/* eslint new-cap: ["error", { "capIsNewExceptions": ["Schema"] }] */
const truckSchema = mongoose.Schema({
  created_by: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'User',
  },
  assigned_to: String,
  type: {
    type: String,
    required: [true, 'Please add a type value'],
  },
  status: String,
  created_date: Date,
}, {versionKey: false});

module.exports = mongoose.model('Truck', truckSchema);
