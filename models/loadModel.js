const mongoose = require('mongoose');

/* eslint new-cap: ["error", { "capIsNewExceptions": ["Schema"] }] */
const loadSchema = mongoose.Schema({
  created_by: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'User',
  },
  assigned_to: String,
  status: String,
  state: String,
  name: String,
  payload: Number,
  pickup_address: String,
  delivery_address: String,
  dimensions: Object,
  logs: [{
    message: String,
    time: Date,
    _id: false,
  }],
  created_date: Date,
}, {versionKey: false});

module.exports = mongoose.model('Load', loadSchema);
