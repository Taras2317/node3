const mongoose = require('mongoose');

/* eslint new-cap: ["error", { "capIsNewExceptions": ["Schema"] }] */
const userSchema = mongoose.Schema({
  email: {
    type: String,
    required: [true, 'Please add an email'],
    unique: true,
  },
  password: {
    type: String,
    required: [true, 'Please add a password'],
  },
  role: {
    type: String,
    required: [true, 'Please add a role'],
  },
  created_date: Date,
}, {versionKey: false});

module.exports = mongoose.model('User', userSchema);
